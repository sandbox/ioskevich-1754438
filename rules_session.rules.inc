<?php

// hook_rules_action_info

function rules_session_rules_action_info() {
  return array(
    'add_data_to_session' => array(
      'label' => t('Store value to $_SESSION'),
      'parameter' => array(
				'data_key' => array(
					'type' => 'token', 
					'label' => t('Key to identify your data'),
					'description' => t('Note: if the data with this key already exists in the $_SESSION array, it will be overwritten!'),
					'default mode' => 'input'
				),
        'data' => array(
					'type' => array(
						'text', 
						'integer', 
						'decimal', 
						'date', 
						'boolean', 
						'list'
					), 
					'label' => t('Data to be saved with the key provided'),
					'default mode' => 'input'
				),
      ),
      'group' => t('Rules Session'),
			'provides' => array(
				'session_data' => array(
					'label' => t('Stored session data.'),
					'type' => '*',
				)
			),
      'callbacks' => array(
				'execute' => '_rules_session_save_data_to_session',
      ),
		),
		
		'load_data_from_session' => array(
      'label' => t('Load value from $_SESSION'),
      'parameter' => array(
				'data_key' => array(
					'type' => 'token', 
					'label' => t('Key to identify data'),
					'default mode' => 'input'
				),
      ),
      'group' => t('Rules Session'),
			'provides' => array(
				'loaded_session_data' => array(
					'label' => t('Loaded session data.'),
					'type' => 'text',
				)
			),
      'callbacks' => array(
				'execute' => '_rules_session_load_data_from_session',
      ),
		),		
		
		'remove_data_from_session' => array(
			'label' => t('Remove value from $_SESSION'),
			'parameter' => array(
				'data_key' => array(
					'type' => 'token', 
					'label' => t('Key to identify your data'),
					'default mode' => 'input'
				),
			),
			'group' => t('Rules Session'),
			'callbacks' => array(
				'execute' => '_rules_session_remove_data_from_session',
			),
    ),
  );
}

function _rules_session_save_data_to_session($data_key, $data_value, $settings){
	$_SESSION[$data_key] = $data_value;
	return array('session_data' => array($data_key => $data_value));
}

function _rules_session_load_data_from_session($data_key, $settings){
	if(isset($_SESSION[$data_key])){
		return array('loaded_session_data' => $_SESSION[$data_key]);
	}
	return FALSE;
}

function _rules_session_remove_data_from_session($data_key, $settings){
	if(isset($_SESSION[$data_key])){
		unset($_SESSION[$data_key]);	
	}
}

// Hook hook_rules_condition_info
function rules_session_rules_condition_info() {
  return array(
    'session_key_exists' => array(
      'label' => t('$_SESSION key exists'),
      'parameter' => array(
				'data_key' => array(
					'type' => 'token', 
					'label' => t('$_SESSION key to check'),
					'default mode' => 'input'
				),
      ),
			'callbacks' => array(
				'execute' => '_rules_session_key_exists',
      ),
      'group' => t('Rules Session'),
    ),
  );
}

function _rules_session_key_exists($data_key, $data_value, $settings){
	if(isset($_SESSION[$data_key])){
		return TRUE;	
	}
	return FALSE;
}